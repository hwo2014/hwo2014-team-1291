import json
import socket
import sys
import random

class Bot(object):

	
	def __init__(self, socket, name, key):		
		self.thrustAccel = 1.0
		self.thrustBreak = 0.0
		self.thrust = self.thrustAccel
		self.minimaLangle = 1
		self.lastAngle = 0
		self.factor = 0.75
		self.socket = socket
		self.name = name
		self.key = key

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send(msg + "\n")

	def join(self):
		return self.msg("join", {"name": self.name,
								 "key": self.key})

	def throttle(self, throttle):
		self.msg("throttle", throttle)

	def ping(self):
		self.msg("ping", {})

	def run(self):
		self.join()
		self.msg_loop()

	def on_join(self, data):
		print("Joined")
		self.ping()

	def on_game_start(self, data):
		print("Race started")
		self.ping()

	def on_car_positions(self, data):
		for c in data:
			if c['id']['name'] == 'bextrulla':
				angle = abs(c['angle'])
				break
		if angle > self.lastAngle:
			self.thrust = self.thrustBreak
		else:
			self.thrust = self.thrustAccel
		self.lastAngle = angle
		print angle, self.thrust
		
		if random.random() < 0.05:
			self.msg('switchLane', random.choice(['Left', 'Right']))
		
		self.throttle(self.thrust)

	def on_crash(self, data):
		print("Someone crashed")
		self.ping()

	def on_game_end(self, data):
		print("Race ended")
		self.ping()

	def on_error(self, data):
		print("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			line = socket_file.readline()


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = Bot(s, name, key)
		bot.run()


